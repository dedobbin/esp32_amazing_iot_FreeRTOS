#pragma once
#include <stdbool.h>
#include <stddef.h>
bool storage_init();
bool storage_get_int(char* key);

/**
 * Returns a string from storage.
 * 
 * @param output Will contain the result.
 * @param key Key to look up.
 * @param len Amount of characters to read.Last space is used for null terminator.
 */
bool storage_get_string(char* output, char* key, size_t len);

/**
 * Starts a 'session' for user to input wifi credentials
 */
void store_wifi_creds_interactive();