#pragma once

#include <stdbool.h>

#define GPIO_OUT GPIO_NUM_14

void led_test();
void gpio_init();
void led_timer_start(unsigned freq_hz);
void led_timer_stop();
bool led_is_on();
void led_on();
void led_off();
int get_timer_freq();