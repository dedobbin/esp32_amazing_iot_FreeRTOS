#pragma once
#include <stdbool.h>

/**
 * Reads (blocking) a line from from uart, or max `max_size`-1 characters.
 * 
 * @note This function is not threadsafe.
 * @note No timeout is implemented.
 *      should/could be called in a task which is killed after a certain amount of time.
 * 
 * @param output Will contain the result.
 * @param max_size Maximum number -1 of characters to read to prevent buffer overflow.
 *      Last space is used for null terminator.
 */
bool get_line(char* output, int max_size);