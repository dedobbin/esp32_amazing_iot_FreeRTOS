#include <stdint.h>
#include <stdio.h>
#include "common.h"
#include "rom/uart.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

bool get_line(char* output, int max_size)
{
    if (max_size < 1){
        // Fails, needs room for \0
        return false;
    }
    uint8_t c;
    char* output_ptr = output;
    for(;;){
        STATUS s = uart_rx_one_char(&c);
        if (s == OK) {
            int size = output_ptr-output+1;
            printf("%c", c);
            fflush(stdout);
            
            if (c == '\n'){
                *output_ptr = '\0';
                return true;
            }
            
            // -1 to account for \0
            if (size > max_size-1){
                *output_ptr = '\0';
                return false;
            }
            *output_ptr++ = c;
        }
        vTaskDelay(50 / portTICK_PERIOD_MS);
    }
}
