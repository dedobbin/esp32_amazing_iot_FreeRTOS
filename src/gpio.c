#include "gpio.h"
#include <stdbool.h>
#include <esp_log.h>
#include "driver/gpio.h"
#include "driver/timer.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
// #include "freertos/queue.h"

//#define LOG_LOCAL_LEVEL ESP_LOG_DEBUG

static const char *TAG = "GPIO";
static int timer_freq = 0;

#define TIMER_DIVIDER   (16)
static int timed_led_status = 0;
static TaskHandle_t flip_led_task = NULL;

// extern var
SemaphoreHandle_t status_change_semaphore;

static bool IRAM_ATTR timer_group_isr_callback(void * args) {
    timed_led_status = !timed_led_status;
    return timed_led_status;
}

void led_test()
{
    gpio_pad_select_gpio(GPIO_OUT);
    gpio_set_direction(GPIO_OUT, GPIO_MODE_OUTPUT);
    gpio_set_level(GPIO_OUT, 1);
    vTaskDelay(1000);
    gpio_set_level(GPIO_OUT, 0);
    vTaskDelay(1000);
}

void gpio_init()
{
    gpio_pad_select_gpio(GPIO_OUT);
    gpio_set_direction(GPIO_OUT, GPIO_MODE_OUTPUT);
    gpio_set_level(GPIO_OUT, 0);
}

void led_on()
{
    timed_led_status = 1;
    gpio_set_level(GPIO_OUT, 1);
    ESP_LOGD(TAG, "led_on will give semaphore");
    xSemaphoreGive(status_change_semaphore);
}

void led_off()
{
    timed_led_status = 0;
    gpio_set_level(GPIO_OUT, 0);
    ESP_LOGD(TAG, "led_off will give semaphore");
    xSemaphoreGive(status_change_semaphore);
}

void flip_led_loop( void * parameter) 
{
    static int last = -1;
    while(1){
        if (last != timed_led_status){
            gpio_set_level(GPIO_OUT, timed_led_status);
            ESP_LOGD(TAG, "timer_group_isr_callback will give semaphore");
            xSemaphoreGive(status_change_semaphore);
        }
    }
}

void led_timer_start(unsigned freq_hz)
{
    xTaskCreatePinnedToCore(
        flip_led_loop, 
        "flip_led", 
        4096, 
        NULL, 
        tskIDLE_PRIORITY, 
        &flip_led_task, 
        0 
    );

    if (freq_hz <= 0){
        led_timer_stop();
        return;
    }

    timer_config_t config = {
        .divider = TIMER_DIVIDER,
        .counter_dir = TIMER_COUNT_UP,
        .counter_en = TIMER_PAUSE,
        .alarm_en = TIMER_ALARM_EN,
        .auto_reload = TIMER_AUTORELOAD_EN
    };

    uint64_t timer_interval = (TIMER_BASE_CLK / config.divider) / freq_hz;
    //TODO: check for errors
    timer_init(TIMER_GROUP_0, TIMER_0, &config);
    timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0);
    timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, timer_interval);
    timer_enable_intr(TIMER_GROUP_0, TIMER_0);
    timer_isr_callback_add(TIMER_GROUP_0, TIMER_0, timer_group_isr_callback, NULL, 0);
    timer_start(TIMER_GROUP_0, TIMER_0);

    timer_freq = freq_hz;
    ESP_LOGD(TAG, "led_timer_start will give semaphore");
    xSemaphoreGive(status_change_semaphore);
}

void led_timer_stop()
{
    if (timer_freq <= 0)
        return;
    

    timer_deinit(TIMER_GROUP_0, TIMER_0);
    gpio_set_level(GPIO_OUT, 0);
    timer_freq = 0;
    timed_led_status = 0;
    vTaskDelete(flip_led_task);
    ESP_LOGD(TAG, "led_timer_stop will give semaphore");
    xSemaphoreGive(status_change_semaphore);
}

bool led_is_on(){
    return timed_led_status;
}

int get_timer_freq(){
    return timer_freq;
}