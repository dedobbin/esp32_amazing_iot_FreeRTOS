#include "server.h"
#include "gpio.h"
#include "esp_websocket_client.h"
#include <esp_http_server.h>
#include <cJSON.h>
#include <esp_log.h>
#include "freertos/semphr.h"

//#define LOG_LOCAL_LEVEL ESP_LOG_DEBUG

#if !CONFIG_HTTPD_WS_SUPPORT
#error Needs build with HTTPD_WS_SUPPORT enabled in esp-http-server component configuration
#endif

static const char *TAG = "Server";

#define MAX_REQUEST_SIZE 100
#define MAX_WS_CLIENTS 10

static httpd_handle_t server = NULL;
static TaskHandle_t monitor_server_task = NULL;

// extern var
SemaphoreHandle_t status_change_semaphore;

static void ws_broadcast(char* data);
void server_start();
void server_end();
// Used by websockets to respond.
struct async_resp_arg {
    httpd_handle_t hd;
    int fd;
    char* data;
};

// const by default should be stored in flash, no need for progmem etc
const char html_home[] = {
  "<!DOCTYPE HTML><html>"
  "<head>"
  "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
  "<script src=\"script.js\"></script>"
  "<link rel=\"icon\" href=\"data:,\">"
  "</head><body>"
    "<h1>Amazing IoT dashboard</h1>"
    "<div>"
      "Blink frequency <input type=\"text\" value=\"1\"id=\"input-led-freq\"></input>"
      "<button id=\"timer-btn\">Start</button>"
      "<button id=\"timer-stop-btn\">Stop</button>"
      "<br/>"
      "<button id=\"trigger-btn\">trigger (hold)</button>"
    "</div>"
    "<br/>"
    "<div>"
      "<p class=\"led-state\">LED: <span id=\"led-state\"></span></p>"
      "<p class=\"timer-freq\">timer frequency: <span id=\"timer-freq\"></span></p>"
    "</div>"
  "</body>"
  "</html>"
};

const char js_home[] = {
    "var gateway = `ws://${window.location.hostname}/ws`;"
    "var websocket;"
    "window.addEventListener('load', onLoad);"

    "function initWebSocket() {"
    "  console.log('[DEBUG] Trying to open a WebSocket connection...');"
    "  websocket = new WebSocket(gateway);"
    "  websocket.onopen    = on_open;"
    "  websocket.onclose   = on_close;"
    "  websocket.onmessage = on_message;"
    "}"

    "function click_handler(){"
    "  const url = '/led';"
    "  let freq = event.target.id == 'timer-stop-btn' ? 0 : Number(document.getElementById('input-led-freq').value);"
    "  fetch(url, {"
    "    method: 'POST',"
    "    body: JSON.stringify({frequency: freq})"
    "  }).then(response => {"
    "    if (response.status != 200){"
    "      response.text().then(body =>{"
    "        console.error('Invalid: ', response.status, body);"
    "        alert('Invalid request: ' + body);"
    "      })"
    "    }"
    "  });"
    "}"

    "function on_open(event) {"
    "   console.log('WS connection opened');"
    "}"

    "function on_close(event) {"
    "  console.log('WS connection closed');"
    "  setTimeout(initWebSocket, 2000);"
    "}"

    "function on_message(event) {"
//  "  console.log('WS message', event.data);"
    "  const data = JSON.parse(event.data);"
    "  if (data.type == 'server_status'){"
    "    document.getElementById('led-state').innerHTML = (data.led ? 'ON' : 'OFF');"
    "    document.getElementById('timer-stop-btn').disabled = !data.timer;"
    "    document.getElementById('timer-freq').innerHTML = data.timer;"
    "  }"
    "}"

    "function onLoad(event) {"
    "  document.getElementById('timer-btn').addEventListener('click', click_handler);"
    "  document.getElementById('timer-stop-btn').addEventListener('click', click_handler);"
    "  document.getElementById('trigger-btn').addEventListener('mousedown', ()=>{"
    "    websocket.send(JSON.stringify({type: 'server_request', led: 1}))"
    "  });"
    "  document.getElementById('trigger-btn').addEventListener('mouseup', ()=>{"
    "      websocket.send(JSON.stringify({type: 'server_request', led: 0}))"
    "  });"
    "  document.getElementById('trigger-btn').addEventListener('mouseout', ()=>{"
    "      websocket.send(JSON.stringify({type: 'server_request', led: 0}))"
    "  });"
    "  initWebSocket();"
    "}"
};

void get_status_json(cJSON * out)
{
  cJSON_AddItemToObject(out, "type", cJSON_CreateString("server_status"));
  cJSON_AddItemToObject(out, "led", cJSON_CreateNumber( led_is_on() ));
  cJSON_AddItemToObject(out, "timer", cJSON_CreateNumber( get_timer_freq() ));
  cJSON_AddItemToObject(out, "temperature", cJSON_CreateNumber(0)); //TODO
  cJSON_AddItemToObject(out, "humidity", cJSON_CreateNumber(0)); //TODO
}

void monitor_status( void * parameter) 
{
  //Setting SLEEP_TIME_MS higher makes server more stabile, 
  // but will fail to report fast changes accuratly
  const int SLEEP_TIME_MS = 10;
  ESP_LOGI(TAG, "Starting monitor server task");
  for(;;) {
    cJSON *data = cJSON_CreateObject();
    ESP_LOGD(TAG, "monitor_status will try and take semaphore");
    if (xSemaphoreTake(status_change_semaphore, portMAX_DELAY) == pdTRUE) {         
      ESP_LOGD(TAG, "monitor_status TOOK semaphore");
      get_status_json(data);  
      char *json_str = cJSON_Print(data);
      ws_broadcast(json_str);
      cJSON_Delete(data);
      if (xSemaphoreTake(status_change_semaphore, pdMS_TO_TICKS(SLEEP_TIME_MS)) == pdTRUE) {  
        //ESP_LOGI(TAG, "SPAM");
        vTaskDelay(pdMS_TO_TICKS(SLEEP_TIME_MS*10));
        //TODO: now send status
      }       

    }
  }
}

esp_err_t serve_home(httpd_req_t *req)
{
    httpd_resp_send(req, html_home, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

httpd_uri_t uri_home = {
    .uri      = "/",
    .method   = HTTP_GET,
    .handler  = serve_home,
    .user_ctx = NULL
};

esp_err_t serve_script(httpd_req_t *req)
{
    httpd_resp_set_type(req, "application/javascript");
    httpd_resp_send(req, js_home, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

httpd_uri_t uri_script = {
    .uri      = "/script.js",
    .method   = HTTP_GET,
    .handler  = serve_script,
    .user_ctx = NULL
};

esp_err_t led_freq_handler(httpd_req_t *req)
{
    char content[MAX_REQUEST_SIZE+1];
    size_t recv_size = req->content_len;

    if (recv_size >= MAX_REQUEST_SIZE) {
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Request entity too large");
        return ESP_FAIL;
    }
    int ret = httpd_req_recv(req, content, recv_size);
    if (ret < 0) {
        if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
            // TODO: retry calling httpd_req_recv()
            httpd_resp_send_408(req);
        }
        return ESP_FAIL;
    }
    content[recv_size] = '\0';

    cJSON *root = cJSON_Parse(content);
    if (cJSON_HasObjectItem(root, "frequency") && cJSON_IsNumber(cJSON_GetObjectItem(root, "frequency"))) {
      int frequency = cJSON_GetObjectItem(root, "frequency")->valueint;
      if (frequency <= 0){
        led_timer_stop();
      } if (frequency >= 1000){
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Frequency value too high");
        cJSON_Delete(root);
        return ESP_OK;
      }else {
        led_timer_start(frequency);
      }
      //TODO: wrap ok response in a function
      cJSON *resp = cJSON_CreateObject();
      cJSON_AddItemToObject(resp, "status", cJSON_CreateString("ok"));
      cJSON_AddItemToObject(resp, "frequency", cJSON_CreateNumber(frequency));

      httpd_resp_set_type(req, "application/json");
      char *json_str = cJSON_Print(resp);
      httpd_resp_send(req, json_str, HTTPD_RESP_USE_STRLEN);
      
      cJSON_Delete(resp);
    } else {
      httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Body does not contain a valid frequency value");
    }

    cJSON_Delete(root);

    return ESP_OK;
}

httpd_uri_t uri_led = {
    .uri      = "/led",
    .method   = HTTP_POST,
    .handler  = led_freq_handler,
    .user_ctx = NULL
};

static esp_err_t ws_handler(httpd_req_t *req)
{
    if (req->method == HTTP_GET) {
      ESP_LOGI(TAG, "New websocket connection");
      // Immediately send status to client. 

      cJSON *data = cJSON_CreateObject();
      get_status_json(data);  
      char *json_str = cJSON_Print(data);
      ws_broadcast(json_str); //TODO: would be nicer if only send to this one client
      cJSON_Delete(data);

      return ESP_OK;
    }
    ESP_LOGI(TAG, "Received data from the websocket connection");
    //TODO: handle data from client

    httpd_ws_frame_t ws_pkt;
    uint8_t *buf = NULL;
    memset(&ws_pkt, 0, sizeof(httpd_ws_frame_t));
    ws_pkt.type = HTTPD_WS_TYPE_TEXT;
    /* Set max_len = 0 to get the frame len */
    esp_err_t ret = httpd_ws_recv_frame(req, &ws_pkt, 0);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "httpd_ws_recv_frame failed to get frame len with %d", ret);
        return ret;
    }
    ESP_LOGD(TAG, "frame len is %d", ws_pkt.len);
    if (ws_pkt.len) {
        /* ws_pkt.len + 1 is for NULL termination as we are expecting a string */
        buf = calloc(1, ws_pkt.len + 1);
        if (buf == NULL) {
            ESP_LOGE(TAG, "Failed to calloc memory for buf");
            return ESP_ERR_NO_MEM;
        }
        ws_pkt.payload = buf;
        /* Set max_len = ws_pkt.len to get the frame payload */
        ret = httpd_ws_recv_frame(req, &ws_pkt, ws_pkt.len);
        if (ret != ESP_OK) {
            ESP_LOGE(TAG, "httpd_ws_recv_frame failed with %d", ret);
            free(buf);
            return ret;
        }
        ESP_LOGD(TAG, "Got packet with message: %s", ws_pkt.payload);
        cJSON *root = cJSON_Parse((char*) ws_pkt.payload);
        if (cJSON_HasObjectItem(root, "led") && cJSON_IsNumber(cJSON_GetObjectItem(root, "led"))) {
          int led = cJSON_GetObjectItem(root, "led")->valueint;
          if (led <= 0){
            led_off();
          } else {
            led_on();
          }
        }
    }
    ESP_LOGD(TAG, "Packet type: %d", ws_pkt.type);

    // ret = httpd_ws_send_frame(req, &ws_pkt);
    // if (ret != ESP_OK) {
    //     ESP_LOGE(TAG, "httpd_ws_send_frame failed with %d", ret);
    // }
    free(buf);


    return ESP_OK;
}

static const httpd_uri_t ws = {
    .uri        = "/ws",
    .method     = HTTP_GET,
    .handler    = ws_handler,
    .user_ctx   = NULL,
    .is_websocket = true
};

static void ws_send(void *arg)
{
    struct async_resp_arg *resp_arg = arg;
    httpd_handle_t hd = resp_arg->hd;
    int fd = resp_arg->fd;
    char * data = resp_arg->data;
    httpd_ws_frame_t ws_pkt;
    memset(&ws_pkt, 0, sizeof(httpd_ws_frame_t));
    ws_pkt.payload = (uint8_t*)data;
    ws_pkt.len = strlen(data);
    ws_pkt.type = HTTPD_WS_TYPE_TEXT;

    httpd_ws_send_frame_async(hd, fd, &ws_pkt);
    free(resp_arg);
}

static void ws_broadcast(char* data){
    size_t clients = MAX_WS_CLIENTS;
    int client_fds[MAX_WS_CLIENTS];
    if (httpd_get_client_list(server, &clients, client_fds) == ESP_OK) {
        for (size_t i=0; i < clients; ++i) {
            int sock = client_fds[i];
            if (httpd_ws_get_fd_info(server, sock) == HTTPD_WS_CLIENT_WEBSOCKET) {
                //ESP_LOGI(TAG, "Active client (fd=%d)", sock);
                struct async_resp_arg *resp_arg = malloc(sizeof(struct async_resp_arg));
                resp_arg->hd = server;
                resp_arg->fd = sock;
                resp_arg->data = data;
                if (httpd_queue_work(resp_arg->hd, ws_send, resp_arg) != ESP_OK) {
                    ESP_LOGE(TAG, "httpd_queue_work failed!");
                    free(resp_arg);
                    ESP_LOGE(TAG, "Restarting server and wainting 1s");
                    server_end();
                    server_start();
                    vTaskDelay(pdMS_TO_TICKS(1000));
                    break;
                }
            }
        }
    } else {
        ESP_LOGE(TAG, "httpd_get_client_list failed!");
        return;
    }
}

void server_start()
{
  httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    
  ESP_ERROR_CHECK(httpd_start(&server, &config));

  httpd_register_uri_handler(server, &uri_home);
  httpd_register_uri_handler(server, &uri_script);
  httpd_register_uri_handler(server, &uri_led);
  httpd_register_uri_handler(server, &ws);

  static uint8_t uc_parameter_to_pass;
  xTaskCreatePinnedToCore(
    monitor_status, 
    "monitor_server", 
    4096, 
    &uc_parameter_to_pass, 
    tskIDLE_PRIORITY, 
    &monitor_server_task, 0 
  );
}

void server_end()
{
    if (server)
        httpd_stop(server);
    if (monitor_server_task)
      vTaskDelete( monitor_server_task);
    
}