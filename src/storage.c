#include <string.h>
#include "storage.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "common.h"
#include <esp_log.h>

static const char *TAG = "Storage";

#define STORAGE_NAME "storage"

bool storage_init()
{
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    //ESP_ERROR_CHECK( err );
    return err == ESP_OK;
}

// Always call with max_size > 1, passing 2 will return only 1 character, 
//  this is to account for \0
bool storage_get_string(char* output, char* key, size_t len)
{
    if (len < 1){
        return false;// Fails, needs room for \0
    }

    nvs_handle_t nvs_handle;
    esp_err_t err = nvs_open(STORAGE_NAME, NVS_READWRITE, &nvs_handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
        return false;
    }

    nvs_get_blob(nvs_handle, key, output, &len);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error (%s) obtaining %s from storage!\n", esp_err_to_name(err), key);
        return false;
    }
    output[len] = '\0';
    return true;
}

int32_t get_int(char* key)
{
    nvs_handle_t nvs_handle;
    esp_err_t err = nvs_open("storage", NVS_READWRITE, &nvs_handle);
    
    int32_t result;
    err = nvs_get_i32(nvs_handle, "ssid_len", &result);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error (%s) obtaining ssid_len!\n", esp_err_to_name(err));
        return 0;
    }

    return result;
}

void store_wifi_creds_interactive()
{
    printf("Enter SSID:\n");
    char ssid[100];
    get_line(ssid, 50);
    char pw[100];
    printf("Enter Password:\n");
    get_line(pw, 50);
    // printf("SSID: %s\n", ssid);
    // printf("Password: %s\n", pw);

    //printf("Opening Non-Volatile Storage (NVS) handle... \n");
    nvs_handle_t nvs_handle;
    esp_err_t err = nvs_open(STORAGE_NAME, NVS_READWRITE, &nvs_handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        int32_t ssid_len = strlen(ssid)-1;
        int32_t pw_len = strlen(pw)-1;
        
        err = nvs_set_i32(nvs_handle, "ssid_len", ssid_len);
        if (err != ESP_OK) {
            ESP_LOGE(TAG, "Error (%s) writing ssid_len!\n", esp_err_to_name(err));
        }

        err = nvs_set_i32(nvs_handle, "pw_len", pw_len);
        if (err != ESP_OK) {
            ESP_LOGE(TAG, "Error (%s) writing pw_len!\n", esp_err_to_name(err));
        }

        err = nvs_set_blob(nvs_handle, "ssid", ssid, ssid_len);
        if (err != ESP_OK) {
            ESP_LOGE(TAG, "Error (%s) writing ssid!\n", esp_err_to_name(err));
        }

        err = nvs_set_blob(nvs_handle, "pw", pw, pw_len);
        if (err != ESP_OK) {
            ESP_LOGE(TAG, "Error (%s) writing pw!\n", esp_err_to_name(err));
        }
    }
}