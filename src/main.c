#include <string.h>
#include "common.h"
#include "storage.h"
#include "gpio.h"
#include "wifi.h"
#include "server.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include <esp_log.h>

//#define USE_SSID_CONFIG_HEADER
#ifdef USE_SSID_CONFIG_HEADER
#include "ssid_config.h"
#endif

static const char *TAG = "Main";

static SemaphoreHandle_t enter_wifi_mutex = NULL;
static TaskHandle_t enter_wifi_task = NULL;

extern SemaphoreHandle_t status_change_semaphore;


void set_wifi( void * pvParameters )
{
    printf("Press enter to input wifi ssid and password\n");
    char buffer[100];
    get_line(buffer, 5);
    xSemaphoreTake( enter_wifi_mutex, portMAX_DELAY );
    store_wifi_creds_interactive();
    xSemaphoreGive(enter_wifi_mutex);

    // TODO: this can cause race condition when check if enter_wifi_task is NULL
    enter_wifi_task = NULL;
    vTaskDelete(NULL);
}

void app_main()
{
    esp_log_level_set("*", ESP_LOG_DEBUG);

    status_change_semaphore = xSemaphoreCreateBinary();

    enter_wifi_mutex = xSemaphoreCreateMutex();
    
    bool res = storage_init();
    if (!res) ESP_LOGE(TAG, "Error initializing storage");

    #ifndef USE_SSID_CONFIG_HEADER
    static uint8_t uc_parameter_to_pass; // Static because must life for duration of task

    xTaskCreatePinnedToCore( set_wifi, "set_wifi", 4096, &uc_parameter_to_pass, tskIDLE_PRIORITY, &enter_wifi_task, 0 );
    configASSERT( enter_wifi_task );

    // Wait some time and try to kill the task, if user is still entering wifi creds, lock will be taken
    vTaskDelay(3000 / portTICK_PERIOD_MS);
    if( enter_wifi_task != NULL )
    {
        //printf("--- timer ran out, grabbing lock ---\n");
        xSemaphoreTake( enter_wifi_mutex, portMAX_DELAY );
        //printf("--- deleting task---\n");
        vTaskDelete( enter_wifi_task );
        //printf("--- returning lock ---\n");
        xSemaphoreGive( enter_wifi_mutex );
    }

    char ssid[100], pw[100];
    storage_get_string(ssid, "ssid", 100);
    storage_get_string(pw, "pw", 100);
    wifi_init(ssid, pw);
    #else
    if (!res) ESP_LOGI(TAG, "using ssid config header");
    wifi_init(SSID, SSID_PW);
    #endif
    gpio_init();
    server_start();
    ESP_LOGI(TAG, "Setup done");
    for(;;){
        vTaskDelay(5000 / portTICK_PERIOD_MS);
    }
}